create proc proc1
as select * from specialnist

create proc proc2
as select * from predmeti

create proc create_table
@nazvaspec varchar(50),
@zavkaf varchar(50),
@nomtel char(12)
as 
begin
insert into specialnist values(@nazvaspec, @zavkaf, @nomtel)
end

create proc update_table
@id int,
@nazvaspec varchar(50),
@zavkaf varchar(50),
@nomtel char(12)
as 
begin
update specialnist set NazvaSpecialnosti = @nazvaspec, ZaviduvachKafedri = @zavkaf, NomerTelefonu = @nomtel where ID = @id
end

create proc delete_row
@id int
as 
begin
delete from specialnist where ID = @id
end