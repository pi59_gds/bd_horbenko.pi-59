﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamSql
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void possessionBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.possessionBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.shopSystemDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopSystemDataSet.shopOwner". При необходимости она может быть перемещена или удалена.
            this.shopOwnerTableAdapter.Fill(this.shopSystemDataSet.shopOwner);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopSystemDataSet.shop". При необходимости она может быть перемещена или удалена.
            this.shopTableAdapter.Fill(this.shopSystemDataSet.shop);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "shopSystemDataSet.possession". При необходимости она может быть перемещена или удалена.
            this.possessionTableAdapter.Fill(this.shopSystemDataSet.possession);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            possessionBindingSource.EndEdit();
            possessionTableAdapter.Update(shopSystemDataSet);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            possessionBindingSource.AddNew();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            shopBindingSource.EndEdit();
            shopTableAdapter.Update(shopSystemDataSet);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            shopBindingSource.AddNew();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            shopOwnerBindingSource.EndEdit();
            shopOwnerTableAdapter.Update(shopSystemDataSet);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            shopOwnerBindingSource.AddNew();
        }
    }
}
