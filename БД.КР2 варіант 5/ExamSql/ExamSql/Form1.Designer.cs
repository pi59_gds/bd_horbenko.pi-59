﻿namespace ExamSql
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label idRegistrationLabel;
            System.Windows.Forms.Label contributionLabel;
            System.Windows.Forms.Label dateCreateLabel;
            System.Windows.Forms.Label idOwnerLabel;
            System.Windows.Forms.Label idShopLabel;
            System.Windows.Forms.Label idShopLabel1;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label codeLabel;
            System.Windows.Forms.Label capitalLabel;
            System.Windows.Forms.Label profileShopLabel;
            System.Windows.Forms.Label districtLabel;
            System.Windows.Forms.Label idOwnerLabel1;
            System.Windows.Forms.Label fullNameLabel;
            System.Windows.Forms.Label addressOwnerLabel;
            System.Windows.Forms.Label birthdayLabel;
            System.Windows.Forms.Label districtLabel1;
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.shopSystemDataSet = new ExamSql.shopSystemDataSet();
            this.possessionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.possessionTableAdapter = new ExamSql.shopSystemDataSetTableAdapters.possessionTableAdapter();
            this.tableAdapterManager = new ExamSql.shopSystemDataSetTableAdapters.TableAdapterManager();
            this.possessionDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shopTableAdapter = new ExamSql.shopSystemDataSetTableAdapters.shopTableAdapter();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shopDataGridView = new System.Windows.Forms.DataGridView();
            this.shopOwnerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shopOwnerTableAdapter = new ExamSql.shopSystemDataSetTableAdapters.shopOwnerTableAdapter();
            this.shopOwnerDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idRegistrationTextBox = new System.Windows.Forms.TextBox();
            this.contributionTextBox = new System.Windows.Forms.TextBox();
            this.dateCreateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.idOwnerTextBox = new System.Windows.Forms.TextBox();
            this.idShopTextBox = new System.Windows.Forms.TextBox();
            this.idShopTextBox1 = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.codeTextBox = new System.Windows.Forms.TextBox();
            this.capitalTextBox = new System.Windows.Forms.TextBox();
            this.profileShopTextBox = new System.Windows.Forms.TextBox();
            this.districtTextBox = new System.Windows.Forms.TextBox();
            this.idOwnerTextBox1 = new System.Windows.Forms.TextBox();
            this.fullNameTextBox = new System.Windows.Forms.TextBox();
            this.addressOwnerTextBox = new System.Windows.Forms.TextBox();
            this.birthdayDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.districtTextBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            idRegistrationLabel = new System.Windows.Forms.Label();
            contributionLabel = new System.Windows.Forms.Label();
            dateCreateLabel = new System.Windows.Forms.Label();
            idOwnerLabel = new System.Windows.Forms.Label();
            idShopLabel = new System.Windows.Forms.Label();
            idShopLabel1 = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            codeLabel = new System.Windows.Forms.Label();
            capitalLabel = new System.Windows.Forms.Label();
            profileShopLabel = new System.Windows.Forms.Label();
            districtLabel = new System.Windows.Forms.Label();
            idOwnerLabel1 = new System.Windows.Forms.Label();
            fullNameLabel = new System.Windows.Forms.Label();
            addressOwnerLabel = new System.Windows.Forms.Label();
            birthdayLabel = new System.Windows.Forms.Label();
            districtLabel1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopSystemDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.possessionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.possessionDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopOwnerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopOwnerDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(979, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(idRegistrationLabel);
            this.tabPage1.Controls.Add(this.idRegistrationTextBox);
            this.tabPage1.Controls.Add(contributionLabel);
            this.tabPage1.Controls.Add(this.contributionTextBox);
            this.tabPage1.Controls.Add(dateCreateLabel);
            this.tabPage1.Controls.Add(this.dateCreateDateTimePicker);
            this.tabPage1.Controls.Add(idOwnerLabel);
            this.tabPage1.Controls.Add(this.idOwnerTextBox);
            this.tabPage1.Controls.Add(idShopLabel);
            this.tabPage1.Controls.Add(this.idShopTextBox);
            this.tabPage1.Controls.Add(this.possessionDataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(971, 397);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Владение";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(idShopLabel1);
            this.tabPage2.Controls.Add(this.idShopTextBox1);
            this.tabPage2.Controls.Add(nameLabel);
            this.tabPage2.Controls.Add(this.nameTextBox);
            this.tabPage2.Controls.Add(codeLabel);
            this.tabPage2.Controls.Add(this.codeTextBox);
            this.tabPage2.Controls.Add(capitalLabel);
            this.tabPage2.Controls.Add(this.capitalTextBox);
            this.tabPage2.Controls.Add(profileShopLabel);
            this.tabPage2.Controls.Add(this.profileShopTextBox);
            this.tabPage2.Controls.Add(districtLabel);
            this.tabPage2.Controls.Add(this.districtTextBox);
            this.tabPage2.Controls.Add(this.shopDataGridView);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(971, 397);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Магазин";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // shopSystemDataSet
            // 
            this.shopSystemDataSet.DataSetName = "shopSystemDataSet";
            this.shopSystemDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // possessionBindingSource
            // 
            this.possessionBindingSource.DataMember = "possession";
            this.possessionBindingSource.DataSource = this.shopSystemDataSet;
            // 
            // possessionTableAdapter
            // 
            this.possessionTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.possessionTableAdapter = this.possessionTableAdapter;
            this.tableAdapterManager.shopOwnerTableAdapter = this.shopOwnerTableAdapter;
            this.tableAdapterManager.shopTableAdapter = this.shopTableAdapter;
            this.tableAdapterManager.UpdateOrder = ExamSql.shopSystemDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // possessionDataGridView
            // 
            this.possessionDataGridView.AutoGenerateColumns = false;
            this.possessionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.possessionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.possessionDataGridView.DataSource = this.possessionBindingSource;
            this.possessionDataGridView.Location = new System.Drawing.Point(6, 6);
            this.possessionDataGridView.Name = "possessionDataGridView";
            this.possessionDataGridView.RowHeadersWidth = 51;
            this.possessionDataGridView.RowTemplate.Height = 24;
            this.possessionDataGridView.Size = new System.Drawing.Size(737, 245);
            this.possessionDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "idRegistration";
            this.dataGridViewTextBoxColumn1.HeaderText = "idRegistration";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "contribution";
            this.dataGridViewTextBoxColumn2.HeaderText = "contribution";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 125;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "dateCreate";
            this.dataGridViewTextBoxColumn3.HeaderText = "dateCreate";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "idOwner";
            this.dataGridViewTextBoxColumn4.HeaderText = "idOwner";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "idShop";
            this.dataGridViewTextBoxColumn5.HeaderText = "idShop";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // shopBindingSource
            // 
            this.shopBindingSource.DataMember = "shop";
            this.shopBindingSource.DataSource = this.shopSystemDataSet;
            // 
            // shopTableAdapter
            // 
            this.shopTableAdapter.ClearBeforeFill = true;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(idOwnerLabel1);
            this.tabPage3.Controls.Add(this.idOwnerTextBox1);
            this.tabPage3.Controls.Add(fullNameLabel);
            this.tabPage3.Controls.Add(this.fullNameTextBox);
            this.tabPage3.Controls.Add(addressOwnerLabel);
            this.tabPage3.Controls.Add(this.addressOwnerTextBox);
            this.tabPage3.Controls.Add(birthdayLabel);
            this.tabPage3.Controls.Add(this.birthdayDateTimePicker);
            this.tabPage3.Controls.Add(districtLabel1);
            this.tabPage3.Controls.Add(this.districtTextBox1);
            this.tabPage3.Controls.Add(this.shopOwnerDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(971, 397);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Владелец";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "district";
            this.dataGridViewTextBoxColumn11.HeaderText = "district";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 125;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "profileShop";
            this.dataGridViewTextBoxColumn10.HeaderText = "profileShop";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 125;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "capital";
            this.dataGridViewTextBoxColumn9.HeaderText = "capital";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 125;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "code";
            this.dataGridViewTextBoxColumn8.HeaderText = "code";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 125;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn7.HeaderText = "name";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 125;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "idShop";
            this.dataGridViewTextBoxColumn6.HeaderText = "idShop";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 125;
            // 
            // shopDataGridView
            // 
            this.shopDataGridView.AutoGenerateColumns = false;
            this.shopDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shopDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.shopDataGridView.DataSource = this.shopBindingSource;
            this.shopDataGridView.Location = new System.Drawing.Point(3, 6);
            this.shopDataGridView.Name = "shopDataGridView";
            this.shopDataGridView.RowHeadersWidth = 51;
            this.shopDataGridView.RowTemplate.Height = 24;
            this.shopDataGridView.Size = new System.Drawing.Size(744, 337);
            this.shopDataGridView.TabIndex = 0;
            // 
            // shopOwnerBindingSource
            // 
            this.shopOwnerBindingSource.DataMember = "shopOwner";
            this.shopOwnerBindingSource.DataSource = this.shopSystemDataSet;
            // 
            // shopOwnerTableAdapter
            // 
            this.shopOwnerTableAdapter.ClearBeforeFill = true;
            // 
            // shopOwnerDataGridView
            // 
            this.shopOwnerDataGridView.AutoGenerateColumns = false;
            this.shopOwnerDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shopOwnerDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
            this.shopOwnerDataGridView.DataSource = this.shopOwnerBindingSource;
            this.shopOwnerDataGridView.Location = new System.Drawing.Point(16, 12);
            this.shopOwnerDataGridView.Name = "shopOwnerDataGridView";
            this.shopOwnerDataGridView.RowHeadersWidth = 51;
            this.shopOwnerDataGridView.RowTemplate.Height = 24;
            this.shopOwnerDataGridView.Size = new System.Drawing.Size(675, 214);
            this.shopOwnerDataGridView.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "idOwner";
            this.dataGridViewTextBoxColumn12.HeaderText = "idOwner";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 125;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "fullName";
            this.dataGridViewTextBoxColumn13.HeaderText = "fullName";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 125;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "addressOwner";
            this.dataGridViewTextBoxColumn14.HeaderText = "addressOwner";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 125;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "birthday";
            this.dataGridViewTextBoxColumn15.HeaderText = "birthday";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 125;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "district";
            this.dataGridViewTextBoxColumn16.HeaderText = "district";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 125;
            // 
            // idRegistrationLabel
            // 
            idRegistrationLabel.AutoSize = true;
            idRegistrationLabel.Location = new System.Drawing.Point(53, 260);
            idRegistrationLabel.Name = "idRegistrationLabel";
            idRegistrationLabel.Size = new System.Drawing.Size(103, 17);
            idRegistrationLabel.TabIndex = 1;
            idRegistrationLabel.Text = "id Registration:";
            // 
            // idRegistrationTextBox
            // 
            this.idRegistrationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.possessionBindingSource, "idRegistration", true));
            this.idRegistrationTextBox.Location = new System.Drawing.Point(162, 257);
            this.idRegistrationTextBox.Name = "idRegistrationTextBox";
            this.idRegistrationTextBox.Size = new System.Drawing.Size(200, 22);
            this.idRegistrationTextBox.TabIndex = 2;
            // 
            // contributionLabel
            // 
            contributionLabel.AutoSize = true;
            contributionLabel.Location = new System.Drawing.Point(53, 288);
            contributionLabel.Name = "contributionLabel";
            contributionLabel.Size = new System.Drawing.Size(86, 17);
            contributionLabel.TabIndex = 3;
            contributionLabel.Text = "contribution:";
            // 
            // contributionTextBox
            // 
            this.contributionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.possessionBindingSource, "contribution", true));
            this.contributionTextBox.Location = new System.Drawing.Point(162, 285);
            this.contributionTextBox.Name = "contributionTextBox";
            this.contributionTextBox.Size = new System.Drawing.Size(200, 22);
            this.contributionTextBox.TabIndex = 4;
            // 
            // dateCreateLabel
            // 
            dateCreateLabel.AutoSize = true;
            dateCreateLabel.Location = new System.Drawing.Point(53, 317);
            dateCreateLabel.Name = "dateCreateLabel";
            dateCreateLabel.Size = new System.Drawing.Size(86, 17);
            dateCreateLabel.TabIndex = 5;
            dateCreateLabel.Text = "date Create:";
            // 
            // dateCreateDateTimePicker
            // 
            this.dateCreateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.possessionBindingSource, "dateCreate", true));
            this.dateCreateDateTimePicker.Location = new System.Drawing.Point(162, 313);
            this.dateCreateDateTimePicker.Name = "dateCreateDateTimePicker";
            this.dateCreateDateTimePicker.Size = new System.Drawing.Size(200, 22);
            this.dateCreateDateTimePicker.TabIndex = 6;
            // 
            // idOwnerLabel
            // 
            idOwnerLabel.AutoSize = true;
            idOwnerLabel.Location = new System.Drawing.Point(53, 344);
            idOwnerLabel.Name = "idOwnerLabel";
            idOwnerLabel.Size = new System.Drawing.Size(68, 17);
            idOwnerLabel.TabIndex = 7;
            idOwnerLabel.Text = "id Owner:";
            // 
            // idOwnerTextBox
            // 
            this.idOwnerTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.possessionBindingSource, "idOwner", true));
            this.idOwnerTextBox.Location = new System.Drawing.Point(162, 341);
            this.idOwnerTextBox.Name = "idOwnerTextBox";
            this.idOwnerTextBox.Size = new System.Drawing.Size(200, 22);
            this.idOwnerTextBox.TabIndex = 8;
            // 
            // idShopLabel
            // 
            idShopLabel.AutoSize = true;
            idShopLabel.Location = new System.Drawing.Point(53, 372);
            idShopLabel.Name = "idShopLabel";
            idShopLabel.Size = new System.Drawing.Size(60, 17);
            idShopLabel.TabIndex = 9;
            idShopLabel.Text = "id Shop:";
            // 
            // idShopTextBox
            // 
            this.idShopTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.possessionBindingSource, "idShop", true));
            this.idShopTextBox.Location = new System.Drawing.Point(162, 369);
            this.idShopTextBox.Name = "idShopTextBox";
            this.idShopTextBox.Size = new System.Drawing.Size(200, 22);
            this.idShopTextBox.TabIndex = 10;
            // 
            // idShopLabel1
            // 
            idShopLabel1.AutoSize = true;
            idShopLabel1.Location = new System.Drawing.Point(753, 35);
            idShopLabel1.Name = "idShopLabel1";
            idShopLabel1.Size = new System.Drawing.Size(60, 17);
            idShopLabel1.TabIndex = 1;
            idShopLabel1.Text = "id Shop:";
            // 
            // idShopTextBox1
            // 
            this.idShopTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopBindingSource, "idShop", true));
            this.idShopTextBox1.Location = new System.Drawing.Point(847, 32);
            this.idShopTextBox1.Name = "idShopTextBox1";
            this.idShopTextBox1.Size = new System.Drawing.Size(100, 22);
            this.idShopTextBox1.TabIndex = 2;
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(753, 63);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(47, 17);
            nameLabel.TabIndex = 3;
            nameLabel.Text = "name:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopBindingSource, "name", true));
            this.nameTextBox.Location = new System.Drawing.Point(847, 60);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 22);
            this.nameTextBox.TabIndex = 4;
            // 
            // codeLabel
            // 
            codeLabel.AutoSize = true;
            codeLabel.Location = new System.Drawing.Point(753, 91);
            codeLabel.Name = "codeLabel";
            codeLabel.Size = new System.Drawing.Size(43, 17);
            codeLabel.TabIndex = 5;
            codeLabel.Text = "code:";
            // 
            // codeTextBox
            // 
            this.codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopBindingSource, "code", true));
            this.codeTextBox.Location = new System.Drawing.Point(847, 88);
            this.codeTextBox.Name = "codeTextBox";
            this.codeTextBox.Size = new System.Drawing.Size(100, 22);
            this.codeTextBox.TabIndex = 6;
            // 
            // capitalLabel
            // 
            capitalLabel.AutoSize = true;
            capitalLabel.Location = new System.Drawing.Point(753, 119);
            capitalLabel.Name = "capitalLabel";
            capitalLabel.Size = new System.Drawing.Size(53, 17);
            capitalLabel.TabIndex = 7;
            capitalLabel.Text = "capital:";
            // 
            // capitalTextBox
            // 
            this.capitalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopBindingSource, "capital", true));
            this.capitalTextBox.Location = new System.Drawing.Point(847, 116);
            this.capitalTextBox.Name = "capitalTextBox";
            this.capitalTextBox.Size = new System.Drawing.Size(100, 22);
            this.capitalTextBox.TabIndex = 8;
            // 
            // profileShopLabel
            // 
            profileShopLabel.AutoSize = true;
            profileShopLabel.Location = new System.Drawing.Point(753, 147);
            profileShopLabel.Name = "profileShopLabel";
            profileShopLabel.Size = new System.Drawing.Size(88, 17);
            profileShopLabel.TabIndex = 9;
            profileShopLabel.Text = "profile Shop:";
            // 
            // profileShopTextBox
            // 
            this.profileShopTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopBindingSource, "profileShop", true));
            this.profileShopTextBox.Location = new System.Drawing.Point(847, 144);
            this.profileShopTextBox.Name = "profileShopTextBox";
            this.profileShopTextBox.Size = new System.Drawing.Size(100, 22);
            this.profileShopTextBox.TabIndex = 10;
            // 
            // districtLabel
            // 
            districtLabel.AutoSize = true;
            districtLabel.Location = new System.Drawing.Point(753, 175);
            districtLabel.Name = "districtLabel";
            districtLabel.Size = new System.Drawing.Size(53, 17);
            districtLabel.TabIndex = 11;
            districtLabel.Text = "district:";
            // 
            // districtTextBox
            // 
            this.districtTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopBindingSource, "district", true));
            this.districtTextBox.Location = new System.Drawing.Point(847, 172);
            this.districtTextBox.Name = "districtTextBox";
            this.districtTextBox.Size = new System.Drawing.Size(100, 22);
            this.districtTextBox.TabIndex = 12;
            // 
            // idOwnerLabel1
            // 
            idOwnerLabel1.AutoSize = true;
            idOwnerLabel1.Location = new System.Drawing.Point(62, 235);
            idOwnerLabel1.Name = "idOwnerLabel1";
            idOwnerLabel1.Size = new System.Drawing.Size(68, 17);
            idOwnerLabel1.TabIndex = 1;
            idOwnerLabel1.Text = "id Owner:";
            // 
            // idOwnerTextBox1
            // 
            this.idOwnerTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopOwnerBindingSource, "idOwner", true));
            this.idOwnerTextBox1.Location = new System.Drawing.Point(176, 232);
            this.idOwnerTextBox1.Name = "idOwnerTextBox1";
            this.idOwnerTextBox1.Size = new System.Drawing.Size(200, 22);
            this.idOwnerTextBox1.TabIndex = 2;
            // 
            // fullNameLabel
            // 
            fullNameLabel.AutoSize = true;
            fullNameLabel.Location = new System.Drawing.Point(62, 263);
            fullNameLabel.Name = "fullNameLabel";
            fullNameLabel.Size = new System.Drawing.Size(71, 17);
            fullNameLabel.TabIndex = 3;
            fullNameLabel.Text = "full Name:";
            // 
            // fullNameTextBox
            // 
            this.fullNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopOwnerBindingSource, "fullName", true));
            this.fullNameTextBox.Location = new System.Drawing.Point(176, 260);
            this.fullNameTextBox.Name = "fullNameTextBox";
            this.fullNameTextBox.Size = new System.Drawing.Size(200, 22);
            this.fullNameTextBox.TabIndex = 4;
            // 
            // addressOwnerLabel
            // 
            addressOwnerLabel.AutoSize = true;
            addressOwnerLabel.Location = new System.Drawing.Point(62, 291);
            addressOwnerLabel.Name = "addressOwnerLabel";
            addressOwnerLabel.Size = new System.Drawing.Size(108, 17);
            addressOwnerLabel.TabIndex = 5;
            addressOwnerLabel.Text = "address Owner:";
            // 
            // addressOwnerTextBox
            // 
            this.addressOwnerTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopOwnerBindingSource, "addressOwner", true));
            this.addressOwnerTextBox.Location = new System.Drawing.Point(176, 288);
            this.addressOwnerTextBox.Name = "addressOwnerTextBox";
            this.addressOwnerTextBox.Size = new System.Drawing.Size(200, 22);
            this.addressOwnerTextBox.TabIndex = 6;
            // 
            // birthdayLabel
            // 
            birthdayLabel.AutoSize = true;
            birthdayLabel.Location = new System.Drawing.Point(62, 320);
            birthdayLabel.Name = "birthdayLabel";
            birthdayLabel.Size = new System.Drawing.Size(63, 17);
            birthdayLabel.TabIndex = 7;
            birthdayLabel.Text = "birthday:";
            // 
            // birthdayDateTimePicker
            // 
            this.birthdayDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.shopOwnerBindingSource, "birthday", true));
            this.birthdayDateTimePicker.Location = new System.Drawing.Point(176, 316);
            this.birthdayDateTimePicker.Name = "birthdayDateTimePicker";
            this.birthdayDateTimePicker.Size = new System.Drawing.Size(200, 22);
            this.birthdayDateTimePicker.TabIndex = 8;
            // 
            // districtLabel1
            // 
            districtLabel1.AutoSize = true;
            districtLabel1.Location = new System.Drawing.Point(62, 347);
            districtLabel1.Name = "districtLabel1";
            districtLabel1.Size = new System.Drawing.Size(53, 17);
            districtLabel1.TabIndex = 9;
            districtLabel1.Text = "district:";
            // 
            // districtTextBox1
            // 
            this.districtTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.shopOwnerBindingSource, "district", true));
            this.districtTextBox1.Location = new System.Drawing.Point(176, 344);
            this.districtTextBox1.Name = "districtTextBox1";
            this.districtTextBox1.Size = new System.Drawing.Size(200, 22);
            this.districtTextBox1.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(427, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 39);
            this.button1.TabIndex = 11;
            this.button1.Text = "Обновить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(427, 324);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 39);
            this.button2.TabIndex = 12;
            this.button2.Text = "Добавить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(800, 286);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 39);
            this.button3.TabIndex = 14;
            this.button3.Text = "Добавить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(800, 228);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 39);
            this.button4.TabIndex = 13;
            this.button4.Text = "Обновить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(430, 310);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(101, 39);
            this.button5.TabIndex = 14;
            this.button5.Text = "Добавить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(430, 252);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(101, 39);
            this.button6.TabIndex = 13;
            this.button6.Text = "Обновить";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 441);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopSystemDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.possessionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.possessionDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shopDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopOwnerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shopOwnerDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private shopSystemDataSet shopSystemDataSet;
        private System.Windows.Forms.BindingSource possessionBindingSource;
        private shopSystemDataSetTableAdapters.possessionTableAdapter possessionTableAdapter;
        private shopSystemDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView possessionDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private shopSystemDataSetTableAdapters.shopTableAdapter shopTableAdapter;
        private System.Windows.Forms.BindingSource shopBindingSource;
        private System.Windows.Forms.DataGridView shopDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.TabPage tabPage3;
        private shopSystemDataSetTableAdapters.shopOwnerTableAdapter shopOwnerTableAdapter;
        private System.Windows.Forms.BindingSource shopOwnerBindingSource;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox idRegistrationTextBox;
        private System.Windows.Forms.TextBox contributionTextBox;
        private System.Windows.Forms.DateTimePicker dateCreateDateTimePicker;
        private System.Windows.Forms.TextBox idOwnerTextBox;
        private System.Windows.Forms.TextBox idShopTextBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox idShopTextBox1;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox codeTextBox;
        private System.Windows.Forms.TextBox capitalTextBox;
        private System.Windows.Forms.TextBox profileShopTextBox;
        private System.Windows.Forms.TextBox districtTextBox;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox idOwnerTextBox1;
        private System.Windows.Forms.TextBox fullNameTextBox;
        private System.Windows.Forms.TextBox addressOwnerTextBox;
        private System.Windows.Forms.DateTimePicker birthdayDateTimePicker;
        private System.Windows.Forms.TextBox districtTextBox1;
        private System.Windows.Forms.DataGridView shopOwnerDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
    }
}

