create database shopSystem
go

use shopSystem
go

create table shop
(
    idShop    int identity (1,1) not null primary key,
	name varchar(255),
    code   int unique not null,
    capital int check (capital > 0),
	profileShop varchar(255),
	district varchar(255)
	
)
go

create table shopOwner
(
    idOwner int identity (1,1) not null primary key,
	fullName varchar(255),
	addressOwner varchar(255),
	birthday date,
	district varchar(100)
)
go

create table possession
(
	idRegistration int identity (1,1) not null primary key,
    contribution int check (contribution > 0),
	dateCreate date,
	idOwner int references shopOwner (idOwner),
	idShop int references shop (idShop)
)
go

insert into shop(name,code,capital,profileShop,district)
values('������� �������',1122,100000,'������� ������� ��� ���� � ������� ������������','������� ���. ������������ 18'),
('�����������',1312,1100000,'������� ��������� ����� Ford �� Fiat','������� ���. ���������� 54'),
('����������',9812,980000,'������� �����,������, ������� �� ����','��� ���. ����������� 11')


insert into shopOwner(fullName,addressOwner,birthday,district)
values('������ ������ ���������','��� ���. ��� ������� 12','21/03/1980','��������'),
('������ ���� ��������','������� ���. ���������� 58','11/02/1970','����������'),
('������ ����� ³��������','������� ���. ������������ 11','1/02/1993','�������')


insert into possession(contribution,dateCreate,idOwner,idShop)
values(50000,'10/11/2019',1,1),
(120000,'10/02/2019',1,2),
(90000,'1/02/2014',3,3)

---------------
--user
use shopSystem
go 

create login shopManager with password = 'shopManager'
use shopSystem
create user shopManager for login shopManager

create role shopManagerDB authorization shopManager
alter role shopManagerDB add member shopManager


grant select on shop to shopManagerDB
grant select on shopOwner to shopManagerDB
grant select on possession to shopManagerDB

--------------------
--backup

backup database shopSystem
to disk = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\shopSystemDB.bak'
with init,
name = 'shopSystem Full Db backup',
description = 'shopSystem Full Database Backup'

restore database shopSystem
from disk = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\shopSystemDB.bak'
with recovery,replace
