--1
create login dekans_log with password = '12345';
use rozkladuniversity;
create user decans for login dekans_log;
create role decans_role authorization decans;
alter role decans_role add member decans;

grant select, alter, delete on specialnist to decans_role;
grant select on semestr to decans_role;
grant select, alter, delete on vikladachi to decans_role;
grant select, alter, insert, delete, update on predmeti to decans_role;
grant select, alter on kurs to decans_role;


--2
exec sp_addlogin @loginame = 'vikl_login', @passwd = '123456', @defdb = 'rozkladuniversity';
use rozkladuniversity;
exec sp_adduser @loginame = 'vikl_login', @name_in_db = 'vikl';
exec sp_addrole @rolename = 'vikl_role', @ownername = 'vikl';
exec sp_addrolemember @rolename = 'vikl_role', @membername = 'vikl';

grant select on specialnist to vikl_role;
grant select on semestr to vikl_role;
grant select on vikladachi to vikl_role;
grant select, alter on predmeti to vikl_role;
grant select on kurs to vikl_role;


