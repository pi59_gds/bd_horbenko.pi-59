--1
select @@SERVERNAME as [Server\Instance]; 
select @@VERSION as SQLServerVersion; 
select @@ServiceName AS ServiceInstance;
select DB_NAME() AS CurrentDB_Name;

select  @@Servername AS ServerName ,
        create_date AS  ServerStarted ,
        DATEDIFF(s, create_date, GETDATE()) / 86400.0 AS DaysRunning ,
        DATEDIFF(s, create_date, GETDATE()) AS SecondsRunnig
from    sys.databases
where   name = 'tempdb'; 

exec sp_helpserver; 
--Or
exec sp_linkedservers; 
--Or
select  @@SERVERNAME AS Server ,
        Server_Id AS  LinkedServerID ,
        name AS LinkedServer ,
        Product ,
        Provider ,
        Data_Source ,
        Modify_Date
from    sys.servers
order by name;

exec sp_helpdb; 
--Or 
exec sp_Databases; 
--Or 
select  @@SERVERNAME AS Server ,
        name AS DBName ,
        recovery_model_Desc AS RecoveryModel ,
        Compatibility_level AS CompatiblityLevel ,
        create_date ,
        state_desc
from    sys.databases
order by Name; 

select  @@SERVERNAME AS Server ,
        d.name AS DBName ,
        create_date ,
        compatibility_level ,
        m.physical_name AS FileName
from    sys.databases d
        JOIN sys.master_files m ON d.database_id = m.database_id
where   m.[type] = 0 -- data files only
order by d.name;

--2
use reiting;
select  *
from    sys.objects
where   type = 'U';

exec sp_Helpfile; 
--Or 
select  @@Servername AS Server ,
        DB_NAME() AS DB_Name ,
        File_id ,
        Type_desc ,
        Name ,
        LEFT(Physical_Name, 1) AS Drive ,
        Physical_Name ,
        RIGHT(physical_name, 3) AS Ext ,
        Size ,
        Growth
from    sys.database_files
order by File_id;

--3
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        o.name as ViewName ,
        o.[Type] ,
        o.create_date
from    sys.objects o
where   o.[Type] = 'V'
order by o.name  
--Or
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        name as ViewName ,
        create_date
from    sys.Views
order by name 
--Or
select  @@Servername as ServerName ,
        TABLE_CATALOG ,
        TABLE_SCHEMA ,
        TABLE_NAME ,
        TABLE_TYPE
from     INFORMATION_SCHEMA.TABLES
where   TABLE_TYPE = 'VIEW'
order by TABLE_NAME 
--Or
select  @@Servername as ServerName ,
        DB_NAME() as DB_Name ,
        o.name as 'ViewName' ,
        o.Type ,
        o.create_date ,
        sm.[DEFINITION] as 'View script'
from    sys.objects o
        INNER JOIN sys.sql_modules sm on o.object_id = sm.OBJECT_ID
where   o.Type = 'V' 
order by o.name;

--4
select  @@Servername as ServerName ,
        DB_NAME() as DB_Name ,
        o.name as 'Functions' ,
        o.[Type] ,
        o.create_date
from    sys.objects o
where   o.Type = 'FN'
order by o.name;
--Or
select  @@Servername as ServerName ,
        DB_NAME() as DB_Name ,
        o.name as 'FunctionName' ,
        o.[type] ,
        o.create_date ,
        sm.[DEFINITION] as 'Function script'
from    sys.objects o
        INNER JOIN sys.sql_modules sm on o.object_id = sm.OBJECT_ID
where   o.[Type] = 'FN' 
order by o.name;

--5
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        parent.name as TableName ,
        o.name as TriggerName ,
        o.[Type] ,
        o.create_date
from    sys.objects o
        INNER JOIN sys.objects parent on o.parent_object_id = parent.object_id
where   o.Type = 'TR' 
order by parent.name ,
        o.name 
--Or 
select  @@Servername as ServerName ,
        DB_NAME() as DB_Name ,
        Parent_id ,
        name as TriggerName ,
        create_date
from    sys.triggers
where   parent_class = 1
order by name;
--Or 
select  @@Servername as ServerName ,
        DB_NAME() as DB_Name ,
        OBJECT_NAME(Parent_object_id) as TableName ,
        o.name as 'TriggerName' ,
        o.Type ,
        o.create_date ,
        sm.[DEFINITION] as 'Trigger script'
from    sys.objects o
        INNER JOIN sys.sql_modules sm on o.object_id = sm.OBJECT_ID
where   o.Type = 'TR'
order by o.name;

--6
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        OBJECT_SCHEMA_NAME(object_id) as SchemaName ,
        OBJECT_NAME(object_id) as Tablename ,
        Column_id ,
        name as  Computed_Column ,
        [Definition] ,
        is_persisted
from    sys.computed_columns
order by SchemaName ,
        Tablename ,
        [Definition]; 
--Or  
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        OBJECT_SCHEMA_NAME(t.object_id) as SchemaName,
        t.name as TableName ,
        c.Column_ID as Ord ,
        c.name as Computed_Column
from    sys.Tables t
        INNER JOIN sys.Columns c on t.object_id = c.object_id
where   is_computed = 1
order by t.name ,
        SchemaName ,
        c.Column_ID

--7
exec sp_tables; 
--Or 
select  @@Servername AS ServerName ,
        TABLE_CATALOG ,
        TABLE_SCHEMA ,
        TABLE_NAME
from     INFORMATION_SCHEMA.TABLES
where   TABLE_TYPE = 'BASE TABLE'
order by TABLE_NAME ;
--Or
select  @@Servername AS ServerName ,
        DB_NAME() AS DBName ,
        o.name AS 'TableName' ,
        o.[Type] ,
        o.create_date
from    sys.objects o
where   o.Type = 'U' -- User table 
order by o.name;
--Or 
select  @@Servername AS ServerName ,
        DB_NAME() AS DBName ,
        t.Name AS TableName,
        t.[Type],
        t.create_date
from    sys.tables t
order by t.Name;

select  'Select ''' + DB_NAME() + '.' + SCHEMA_NAME(SCHEMA_ID) + '.'
        + LEFT(o.name, 128) + ''' as DBName, count(*) as Count From ' + SCHEMA_NAME(SCHEMA_ID) + '.' + o.name
        + ';' AS ' Script generator to get counts for all tables'
from    sys.objects o
where   o.[type] = 'U'
order by o.name;

select  @@ServerName as ServerName ,
        DB_NAME() as DBName ,
        OBJECT_NAME(ddius.object_id) as TableName ,
        SUM(ddius.user_seeks + ddius.user_scans + ddius.user_lookups)
                                                               as  Reads ,
        SUM(ddius.user_updates) as Writes ,
        SUM(ddius.user_seeks + ddius.user_scans + ddius.user_lookups
            + ddius.user_updates) as [Reads&Writes] ,
        ( select    DATEDIFF(s, create_date, GETDATE()) / 86400.0
          from      master.sys.databases
          where     name = 'tempdb'
        ) as SampleDays ,
        ( select    DATEDIFF(s, create_date, GETDATE()) as SecoundsRunnig
          from      master.sys.databases
          where     name = 'tempdb'
        ) as SampleSeconds
from    sys.dm_db_index_usage_stats ddius
        INNER JOIN sys.indexes i on ddius.object_id = i.object_id
                                     AND i.index_id = ddius.index_id
where    OBJECTPROPERTY(ddius.object_id, 'IsUserTable') = 1
        AND ddius.database_id = DB_ID()
group by OBJECT_NAME(ddius.object_id)
order by [Reads&Writes] desc;

declare DBNameCursor cursor
for
    select  Name
    from    sys.databases
    where    Name NOT IN ( 'master', 'model', 'msdb', 'tempdb',
                            'distribution' )
    order by Name; 
declare @DBName NVARCHAR(128) 
declare @cmd VARCHAR(4000) 
if OBJECT_ID(N'tempdb..TempResults') IS NOT NULL
    begin 
        drop table tempdb..TempResults 
    end 
create table tempdb..TempResults
    (
      ServerName NVARCHAR(128) ,
      DBName NVARCHAR(128) ,
      TableName NVARCHAR(128) ,
      Reads INT ,
      Writes INT ,
      ReadsWrites INT ,
      SampleDays DECIMAL(18, 8) ,
      SampleSeconds INT
    ) 
open DBNameCursor 
fetch next from DBNameCursor into @DBName 
while @@fetch_status = 0
    begin 
        select   @cmd = 'Use ' + @DBName + '; ' 
        select   @cmd = @cmd + ' Insert Into tempdb..TempResults 
select @@ServerName AS ServerName, 
DB_NAME() AS DBName, 
object_name(ddius.object_id) AS TableName , 
SUM(ddius.user_seeks 
+ ddius.user_scans 
+ ddius.user_lookups) AS Reads, 
SUM(ddius.user_updates) as Writes, 
SUM(ddius.user_seeks 
+ ddius.user_scans 
+ ddius.user_lookups 
+ ddius.user_updates) as ReadsWrites, 
(SELECT datediff(s,create_date, GETDATE()) / 86400.0 
from sys.databases WHERE name = ''tempdb'') as SampleDays, 
(SELECT datediff(s,create_date, GETDATE()) 
from sys.databases WHERE name = ''tempdb'') as SampleSeconds 
from sys.dm_db_index_usage_stats ddius 
inner join sys.indexes i
on ddius.object_id = i.object_id 
and i.index_id = ddius.index_id 
where objectproperty(ddius.object_id,''IsUserTable'') = 1 --True 
and ddius.database_id = db_id() 
group by object_name(ddius.object_id) 
order by ReadsWrites desc;' 
        execute (@cmd) 
        fetch next from DBNameCursor into @DBName 
    end 
close DBNameCursor 
deallocate DBNameCursor 
select  *
from    tempdb..TempResults
order by DBName ,
        TableName;

--8
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        parent.name as 'TableName' ,
        o.name as 'Constraints' ,
        o.[Type] ,
        o.create_date
from    sys.objects o
        INNER JOIN sys.objects parent
               ON o.parent_object_id = parent.object_id
where   o.Type = 'C' 
order by parent.name ,
        o.name 
--Or
select  @@Servername as ServerName ,
        DB_NAME() as DBName ,
        OBJECT_SCHEMA_NAME(parent_object_id) as SchemaName ,
        OBJECT_NAME(parent_object_id) as TableName ,
        parent_column_id as  Column_NBR ,
        Name as  CheckConstraintName ,
        type ,
        type_desc ,
        create_date ,
        OBJECT_DEFINITION(object_id) as CheckConstraintDefinition
from    sys.Check_constraints
order by TableName ,
        SchemaName ,
        Column_NBR 