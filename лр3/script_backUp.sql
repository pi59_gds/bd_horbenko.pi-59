BACKUP DATABASE RozkladUniversity
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_FullDbBkup.bak' WITH INIT, NAME = 'RozkladUniversity Full Db backup',
DESCRIPTION = 'RozkladUniversity Full Database Backup'

RESTORE DATABASE RozkladUniversity
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_FullDbBkup.bak'
WITH RECOVERY, REPLACE

BACKUP DATABASE RozkladUniversity
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_FullDbBkup.bak' WITH INIT, NAME = 'RozkladUniversity Full Db backup',
DESCRIPTION = 'RozkladUniversity Full Database Backup'

BACKUP LOG RozkladUniversity
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_TlogBkup.bak'
WITH NOINIT, NAME = 'RozkladUniversity Translog backup',
DESCRIPTION = 'RozkladUniversity Transaction Log Backup', NOFORMAT

BACKUP LOG RozkladUniversity
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_TlogBkup.bak'
WITH NORECOVERY

RESTORE DATABASE RozkladUniversity
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_FullDbBkup.bak'
WITH NORECOVERY

RESTORE LOG RozkladUniversity
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_TlogBkup.bak'
WITH NORECOVERY

RESTORE LOG RozkladUniversity
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_TlogBkup.bak' 
WITH RECOVERY

BACKUP DATABASE RozkladUniversity
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_DiffDbBkup.bak'
WITH INIT, DIFFERENTIAL, NAME = 'rozkladuniversity Diff Db backup',
DESCRIPTION = 'rozkladuniversity Differential Database Backup'

RESTORE DATABASE RozkladUniversity
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\Backup\rozkladuniversity_DiffDbBkup.bak' 
WITH NORECOVERY